var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('misarchivos', { title: 'SISTEMA DE COMPARTICION DE CODIGO EN AULA' });
});

module.exports = router;

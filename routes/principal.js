var express = require('express');
var router = express.Router();

bodyParser = require('body-parser');



router.use(bodyParser.json())

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('principal', 
  	{ 
  		title: 'Ingreso Estudiantes',
  		 usuario: 'estudiante'
  	}
  );
});


router.post("/",  function(req, res, next) {
	var usuario = req.body.rut;
	let pass = req.body.pass;
	var perfil=req.body.perfil;
	console.log(usuario+"  - "+pass+"  - "+perfil);
	
	if (perfil=="docente") {
		console.log("profe");
		res.render('docente/docente', 
	  	  { 
	  		title: 'Ingreso '+perfil,
	  		usuario: usuario,
	  		perfil: perfil

	  	  }
	  	  );
	}else{
		console.log("estudiante");
		res.render('estudiante/estudiante', 
	  	  { 
	  		title: 'Ingreso '+perfil,
	  		usuario: usuario,
	  		perfil: perfil

	  	  }
	  	  );
	}
    
  
});


module.exports = router;

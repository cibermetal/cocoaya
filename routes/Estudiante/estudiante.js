var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('docente', { title: 'SISTEMA DE COMPARTICION DE CODIGO EN AULA', usuario: req.usuario });
});

module.exports = router;